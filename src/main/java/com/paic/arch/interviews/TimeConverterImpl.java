package com.paic.arch.interviews;

/**
 * @version V1.0
 * @package：com.paic.arch.interviews
 * @author：boge【www.xboge.com】
 * @date：2018/2/26-下午10:38
 * @email：boge.x@foxmail.com
 * @Description: 平安普惠在线技术测试题目，主要考察进制转换
 */
public class TimeConverterImpl implements TimeConverter {
    @Override
    public String convertTime(String aTime) {

        char[] time = check(aTime);

        int hour = (time[0] - '0') * 10 + time[1] - '0';
        int min = (time[3] - '0') * 10 + time[4] - '0';
        int sec = (time[6] - '0') * 10 + time[7] - '0';

        StringBuilder sb = new StringBuilder();

        if (sec % 2 == 0) {
            sb.append("Y");
        } else {
            sb.append("O");
        }
        sb.append("\n");

        for (int i = 0,h = hour / 5; i < 4; i++) {
            if (i < h) {
                sb.append("R");
            } else {
                sb.append("O");
            }
        }
        sb.append("\n");

        for (int i = 0,h = hour % 5; i < 4; i++) {
            if (i < h) {
                sb.append("R");
            } else {
                sb.append("O");
            }
        }
        sb.append("\n");

        for (int i = 0,m = min / 5; i < 11; i++) {
            if (i < m) {
                if (i == 2 || i == 5 || i == 8) {
                    sb.append("R");
                } else {
                    sb.append("Y");
                }
            } else {
                sb.append("O");
            }
        }
        sb.append("\n");

        for (int i = 0,m = min % 5; i < 4; i++) {
            if(i < m){
                sb.append("Y");
            }else {
                sb.append("O");
            }
        }

        return sb.toString();
    }

    /**
     * 检查输入值的格式与数值，并返回char数组，考虑到需要检查每个字符的边界规格，转换成char数组效率最快
     */
    private char[] check(String aTime) {
        char[] time = null;
        if (aTime == null || aTime.length() != 8) {
            throw new RuntimeException("the time's length error");
        }

        time = aTime.toCharArray();

        if (time[2] != 58 || time[5] != 58) {
            throw new RuntimeException("the separator error");
        }

        if (time[0] < '0' || time[0] > '2' || time[1] < '0' || time[1] > '4'
                || time[3] < '0' || time[3] > '5' || time[4] < '0' || time[4] > '9'
                || time[6] < '0' || time[6] > '5' || time[7] < '0' || time[7] > '9') {
            throw new RuntimeException("time number error");
        }

        return time;
    }
}
